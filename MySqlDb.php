<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MySqlDb
 *
 * @author iv2
 */
class MySqlDb {
    protected $_mysql;
    protected $_query;
    
    public function __construct($host, $username, $password, $db) 
    {
        $this->_mysql = new mysqli($host, $username, $password, $db) or die('There was a problem connecting...');
    }
    
    protected function _prepareQuery()
    {
        if ( !$stmt = $this->_mysql->prepare($this->_query)) 
        {
            trigger_error('Problem preparing query', E_USER_ERROR);
        }
        return $stmt;
    }
    
    protected function _dynamicBindResults($stmt) {
        $parameters = array();
        $results = array();
        $meta = $stmt->result_metadata();
        
        while ( $field = $meta->fetch_field())
        {
            $parameters[] = &$row[$field->name];
        }
        
        call_user_func_array(array($stmt, 'bind_result'), $parameters);
        
        while ($stmt->fetch())
        {
            $x = array();
            
            foreach($row as $key => $value)
            {
                $x[$key] = $value;
            }
            $results[] = $x;
        }
        return $results;
    }


    public function query($query) 
    {
        $this->_query = filter_var($query, FILTER_SANITIZE_STRING);
        $stmt = $this->_prepareQuery();
        $stmt->execute();
        $results = $this->_dynamicBindResults($stmt);
        print_r($results);
    }
    
    protected function _buildQuery($numRows = NULL, $tableData = false) 
    {
    }


    public function get($tableName, $numRows = NULL) 
    {
        
    }
    
    public function insert($tableName, $insertData)
    {
    
    }
    
    function update($tableName, $tableData)
    {
        
    }
    
    function delete($tableName)
    {
        
    }
    
    function where($whereProp, $whereValue)
    {
        
    }
    
    function __destruct()
    {
        //$this->mysql->close();
    }
}

?>
